*** Settings ***
Documentation     Gitlab & Robot Framework Tutoriaali
Library           SeleniumLibrary
Suite Setup       Open Browser       ${ENV}    ${BROWSER}
Suite Teardown    Close Browser

*** Variables ***
${ENV}        https://yle.fi
${BROWSER}    headlesschrome

*** Test Cases ***
Tarkasta Otsikko
    Title Should Be                Yle.fi - oivalla jotain uutta

Tarkasta Valikko
    Page Should Contain Link       yle-header-main-link-ylefi
    Page Should Contain Element    yle-header-main-link--areena
    Page Should Contain Element    yle-header-main-link--uutiset
    Page Should Contain Element    yle-header-main-link--urheilu