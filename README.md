# Testiautomaation perusteet

Esimerkki testiautomaatiosta, työkaluina [Robot Framework](https://robotframework.org/) ja [docker-robot-framework](https://github.com/ppodgorsek/docker-robot-framework), joka sisältää Robot Frameworkin ja [SeleniumLibrary](https://github.com/robotframework/SeleniumLibrary) -kirjaston.